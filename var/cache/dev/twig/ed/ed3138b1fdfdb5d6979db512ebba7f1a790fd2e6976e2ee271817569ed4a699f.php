<?php

/* @FOSUser/Security/login_content.html.twig */
class __TwigTemplate_048fb93d6a8f39c993b8dbd34fc4712cc8b933aa66439129f274ab40e4d38e86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'fos_user_content' => [$this, 'block_fos_user_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login_content.html.twig"));

        // line 1
        echo "    ";
        // line 2
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_fos_user_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 3
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 4
            echo "        <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", []), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", []), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 6
        echo "    <div class=\"card-body\">
        <form class=\"login-form\" action=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_check");
        echo "\" method=\"post\">
            ";
        // line 8
        if (($context["csrf_token"] ?? $this->getContext($context, "csrf_token"))) {
            // line 9
            echo "                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            echo twig_escape_filter($this->env, ($context["csrf_token"] ?? $this->getContext($context, "csrf_token")), "html", null, true);
            echo "\" />
            ";
        }
        // line 11
        echo "            <div class=\"form-group\">
                <i class=\"fa fa-user\"></i>
                <div class=\"form-label-group\">
                    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"Email address\" required=\"required\" autofocus=\"autofocus\" autocomplete=\"username\">
                    <label for=\"username\">";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.username", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
                </div>
            </div>
            <div class=\"form-group\">
                <i class=\"fa fa-lock\"></i>
                <div class=\"form-label-group\">
                    <input type=\"password\" id=\"Password\" name=\"_password\" class=\"form-control\" placeholder=\"Password\" required=\"required\" autocomplete=\"current-password\"/>
                    <label for=\"password\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.password", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
                </div>
            </div>


            <div class=\"form-group\">
                <div class=\"checkbox\">
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                    <label for=\"remember_me\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.remember_me", [], "FOSUserBundle"), "html", null, true);
        echo "</label>
                </div>
            </div>
            <button class=\"btn btn-primary btn-block\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("security.login.submit", [], "FOSUserBundle"), "html", null, true);
        echo "\">Se Connecter
                <i class=\"m-icon-swapright m-icon-blue\"></i></button>
            <div class=\"forget-password\">
                <h4>Vous avez oublité votre mot de passe  ?</h4>
                <p>
                    Cliquez  <b><a href=\"";
        // line 38
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_request");
        echo "\" id=\"forget-password\">
                            ici </a></b>
                    pour reinitialiser votre mot de passe.
                </p>
            </div>
            <div class=\"create-account\">
                <p>
                    Vous n'avez pas de compte ?&nbsp;  <a href=\"";
        // line 45
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\"><b>Créez un compte</b></a>
                </p>
            </div>
        </form>

    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  123 => 45,  113 => 38,  105 => 33,  99 => 30,  88 => 22,  78 => 15,  74 => 14,  69 => 11,  63 => 9,  61 => 8,  57 => 7,  54 => 6,  48 => 4,  46 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("    {% trans_default_domain 'FOSUserBundle' %}
{% block fos_user_content %}
{% if error %}
        <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}
    <div class=\"card-body\">
        <form class=\"login-form\" action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\">
            {% if csrf_token %}
                <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
            {% endif %}
            <div class=\"form-group\">
                <i class=\"fa fa-user\"></i>
                <div class=\"form-label-group\">
                    <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" class=\"form-control\" placeholder=\"Email address\" required=\"required\" autofocus=\"autofocus\" autocomplete=\"username\">
                    <label for=\"username\">{{ 'security.login.username'|trans }}</label>
                </div>
            </div>
            <div class=\"form-group\">
                <i class=\"fa fa-lock\"></i>
                <div class=\"form-label-group\">
                    <input type=\"password\" id=\"Password\" name=\"_password\" class=\"form-control\" placeholder=\"Password\" required=\"required\" autocomplete=\"current-password\"/>
                    <label for=\"password\">{{ 'security.login.password'|trans }}</label>
                </div>
            </div>


            <div class=\"form-group\">
                <div class=\"checkbox\">
                    <input type=\"checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\" />
                    <label for=\"remember_me\">{{ 'security.login.remember_me'|trans }}</label>
                </div>
            </div>
            <button class=\"btn btn-primary btn-block\" type=\"submit\" id=\"_submit\" name=\"_submit\" value=\"{{ 'security.login.submit'|trans }}\">Se Connecter
                <i class=\"m-icon-swapright m-icon-blue\"></i></button>
            <div class=\"forget-password\">
                <h4>Vous avez oublité votre mot de passe  ?</h4>
                <p>
                    Cliquez  <b><a href=\"{{ path('fos_user_resetting_request') }}\" id=\"forget-password\">
                            ici </a></b>
                    pour reinitialiser votre mot de passe.
                </p>
            </div>
            <div class=\"create-account\">
                <p>
                    Vous n'avez pas de compte ?&nbsp;  <a href=\"{{ path('fos_user_registration_register') }}\"><b>Créez un compte</b></a>
                </p>
            </div>
        </form>

    </div>
{% endblock fos_user_content %}", "@FOSUser/Security/login_content.html.twig", "C:\\wamp64\\www\\app\\app\\Resources\\FOSUserBundle\\views\\Security\\login_content.html.twig");
    }
}
